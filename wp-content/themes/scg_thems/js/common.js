$(function() {

	$('.popup__video').magnificPopup({
		type: 'iframe',
		removalDelay: 350,
		mainClass: 'mfp-zoom-in'
	});

	$('.popup').magnificPopup({
		removalDelay: 500,
		callbacks: {
		    beforeOpen: function() {
		       this.st.mainClass = this.st.el.attr('data-effect');
		    }
		  },
		  midClick: true
	});

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

	$('.banner__slider').owlCarousel({
		nav: true,
		items: 1,
		loop:true
	});

	$('.service__carousel').owlCarousel({
	    center: true,
	    items:3,
	    loop:true,
	    nav: true,
	    margin: 1
	});
});

$(window).load(function() {

	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow");

});

$(window).load(function(){
	$('.grid').masonry({
		columnWidth: 270,
		itemSelector: '.grid__item',
		gutter: 10,
		stamp: '.stamp',
		fitWidth: true
	});
});

$(document).ready(function(){
	$('.popup').click(function() {
       var owl = $('.owl-carousel');
if (owl.children().length > 1) {
  owl.owlCarousel();
} else {
  // apply some CSS classes to fake it
}
		$('.modal__slider').owlCarousel({
			nav: true,
			items: 1,
			loop:true
		});
	});
});
   function sendmail(){ 
           var phone=document.forms["form"]["phone"].value;
           var name=document.forms["form"]["name"].value; 
           var emailtosend=document.forms["form"]["emailtosend"].value;
           var email=document.forms["form"]["email"].value;
     var promise = {
         'phone'        : phone,
         'name'         : name,
         'emailtosend'  : emailtosend,
         'email'        : email
 
};
       console.log(promise);
      if ((phone.length==0) || (name.length==0)){
          document.getElementById("firstnamef").innerHTML="не все поля заполнены";
       
   }else{
        $.ajax({
            type: "GET", 
            url: "/wp-content/themes/scg_thems/controller/sendmail.php", 
            data: promise,
            success: function(date) {
                   document.getElementById("firstname").innerHTML=date;
                   document.getElementById("firstnamef").innerHTML='';
                phone=document.forms["form"]["phone"].value='';
                adress=document.forms["form"]["name"].value='';
                adress=document.forms["form"]["email"].value='';
            }}); }
        
    };