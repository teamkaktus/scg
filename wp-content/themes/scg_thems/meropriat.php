<?php
/*
Template Name: Мероприятия
*/

get_header(); ?>

	</header>

<section class="event clearfix">
		<div class="container">
			<h1 class="page__title">Мероприятия</h1>
		</div>
		<div class="event__wrap">
			<div class="container">
                <div class="event__block grid">
			       <?php
                     $class=array("event__item-bg1", "event__item-bg2", "event__item-bg3", "event__item-bg1", "event__item-bg2", "event__item-bg1", "event__item-bg3", "event__item-bg3", "event__item-bg1", "event__item-bg1");
                     $classsize=array("event__wrapitem-bg1","event__wrapitem-bg2","event__wrapitem-bg3","event__wrapitem-bg4","event__wrapitem-bg5","event__wrapitem-bg6","event__wrapitem-bg7","event__wrapitem-bg8","event__wrapitem-bg9","event__wrapitem-bg10");
            $wp_query = new WP_Query(
                                array( 'cat'  => '4',
                                       'posts_per_page' => 10
                                   ));
           $i=0;

        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                      <?php if($i==7){ ?>
                       </div>
                      <div class="grid">
                       <?php } ?>
                      <a href="#ev-modal<?php echo get_the_ID(); ?>" class="grid__item popup  <?php if($i==4){ ?>grid__item-w1<?php } ?>" data-effect="mfp-zoom-in">
                      <div  class="eevent__item  <?php echo $classsize[$i]; ?>">
                      <span class=" filter <?php echo $class[$i]; ?>"></span> 
						<img  src="<?php echo the_post_thumbnail_url(full); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">
						</div>
						<h4 class="event__title"><?php the_title(); ?></h4>
						<span class="event__descr"><?php echo the_excerpt(); ?></span>
					</a>


        <?php $i++; endwhile; ?>

        <?php wp_reset_postdata(); ?>
        
        <?php if (function_exists("pagination")) {
    pagination($custom_query->max_num_pages);
} ?>
        </div>


			</div>
		</div>
	</section>

	<section class="order">
		<div class="container">
	            <div class="order__title">Хотите заказать себе праздник?</div>
			    <a href="#callback-form" class="order__link popup">Заказать</a>
		</div>
	</section>



<?php get_footer(); ?>
