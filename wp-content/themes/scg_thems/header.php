<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="utf-8">

	<title>SCG</title>
	<meta name="description" content="">

	<meta property="og:image" content="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/Home.jpg">
	<link rel="shortcut icon" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/favicon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/favicon/apple-touch-icon-114x114.png">

	<meta name="viewport" content="width=device-width">

	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/normalize/normilize.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/css/fonts.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/css/main.css">

	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/magnific/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/owlcarousel/owl.carousel.css">

	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/modernizr/modernizr.js"></script>

</head>

<body>
	<?php $wrapper="";  $wrapper2=""; if (is_home()){ $wrapper="";}  ?>
	<?php if ( is_page_template( 'about.php' ) ) { $wrapper="header__about";} ?>
	<?php if ( is_page_template( 'meropriat.php' ) ) { $wrapper="header__inner";  $wrapper2="header__top-inner";} ?>
	<?php if ( is_page_template( 'portfolio.php' ) ) { $wrapper="header__inner";  $wrapper2="header__top-inner";} ?>
	<?php if ( is_page_template( 'yslygu.php' ) ) { $wrapper="header__inner";  $wrapper2="header__top-inner";} ?>
	<?php if ( is_404() ) { $wrapper="header__inner";  $wrapper2="header__top-inner";} ?>
	<?php if (is_singular('post')) { $wrapper="header__inner";  $wrapper2="header__top-inner";} ?>
	<header class="<?php echo $wrapper; ?>">
		<div class="header__top <?php echo $wrapper2; ?>">
			<div class="container">
				<div class="logo">
					<a href="<?php echo esc_url( get_site_url() ); ?>"><?php the_custom_logo(); ?><span style=" width: 220px;
"><?php  echo get_bloginfo('description'); ?></span></a>
				</div>

				<div class="header__contact">
					<a href="tel:<?php echo get_option('site_telephone1'); ?>" class="header__phone"><?php echo get_option('site_telephone1'); ?></a>
					<a href="#callback-form" class="callback__link popup" data-effect="mfp-zoom-in">Обратный звонок</a>
				</div>

				<div class="header__menu">
					<ul class="header__list">
					<?php
                        
                        $args = array(
	'theme_location'  => '', // область темы
	'menu'            => '', // какое меню нужно вставить (по порядку: id, ярлык, имя)
	'container'       => false, 
	'menu_class'      => 'header__list', 
	'echo'            => true, // вывести или записать в переменную
	'items_wrap'      => '<ul class=\"%2$s\">%3$s</ul>', // HTML-шаблон
	'depth'           => 0 // количество уровней вложенности
);
                        wp_nav_menu($args);
                        ?>
					</ul>
				</div>
			</div>
		</div>


