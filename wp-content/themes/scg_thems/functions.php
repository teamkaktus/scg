<?php
/**
 * Magnus functions and definitions
 *
 * @package Magnus
 */

if ( ! function_exists( 'magnus_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function magnus_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Magnus, use a find and replace
	 * to change 'magnus' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'magnus', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 256, 256, true );
	add_image_size( 'magnus-large', 2000, 1500, true  );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
        'primary' => __( 'Primary Sidebar Navigation', 'magnus' ),
				// 'social'  => __( 'Social Links', 'magnus' ),
        'secondary' => __( 'Header Quick Navigation', 'magnus' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
}
endif; // magnus_setup
add_action( 'after_setup_theme', 'magnus_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 * Priority 0 to make it available to lower priority callbacks.
 * @global int $content_width
 */
function magnus_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'magnus_content_width', 1088 );
}
add_action( 'after_setup_theme', 'magnus_content_width', 0 );

/**
 * Register widget area.
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function magnus_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'magnus' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'magnus' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'magnus_widgets_init' );


/**
 * JavaScript Detection.
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 * @since Magnus 2.0
 */
function magnus_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'magnus_javascript_detection', 0 );


/**
 * Google Fonts
 * Gives translators ability to deactivate fonts that don't include their language's characters.
 * @since Magnus 2.0
 */
function magnus_fonts_url() {
    $fonts_url = '/wp-content/themes/Valery_Voschennykova/css/fonts.css';

    return $fonts_url;
}


/**
 * Enqueue scripts and styles.
 */
function magnus_scripts() {
	wp_enqueue_style( 'magnus-fonts', magnus_fonts_url(), array(), null );


	wp_enqueue_script( 'magnus-navigation', get_template_directory_uri() . '/js/navigation.js' );


}
add_action( 'wp_enqueue_scripts', 'magnus_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

//hgjkl;

function mytheme_customize_register( $wp_customize ) {


/*
Добавляем секцию в настройки темы
*/
$wp_customize->add_section(
    // ID
    'data_header_section',
    // Arguments array
    array(
        'title' => 'Контактние данные',
        'capability' => 'edit_theme_options',
        'description' => "Тут можно указать контактние данные"
    )
);

/*
Добавляем поле телефона site_telephone
*/
$wp_customize->add_setting(
    // ID
    'site_telephone1',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_telephone_control1',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Телефон",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_telephone1'
    )
);

/*
Добавляем поле email
*/
$wp_customize->add_setting(
    // ID
    'email',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'email',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "E-mail:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'email'
    )
);
    
    /*
Добавляем поле email
*/
$wp_customize->add_setting(
    // ID
    'adress',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'adress',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "adress:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'adress'
    )
);
    
      /*
Добавляем поле adress2
*/
    
    $wp_customize->add_setting(
    // ID
    'city1',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'city1',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "город №1 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'city1'
    )
);    
$wp_customize->add_setting(
    // ID
    'adress1',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'adress1',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "адреса № 1 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'adress1'
    )
);
/* адрес 2 */
    $wp_customize->add_setting(
    // ID
    'city2',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'city2',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "город №2 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'city2'
    )
);    
$wp_customize->add_setting(
    // ID
    'adress2',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'adress2',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "адреса № 2 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'adress2'
    )
);    
/* адрес 3 */
    $wp_customize->add_setting(
    // ID
    'city3',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'city3',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "город №3 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'city3'
    )
);    
$wp_customize->add_setting(
    // ID
    'adress3',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'adress3',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "адреса № 3 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'adress3'
    )
);    
/* адрес 4 */
    $wp_customize->add_setting(
    // ID
    'city4',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'city4',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "город №4 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'city4'
    )
);    
$wp_customize->add_setting(
    // ID
    'adress4',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'adress4',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "адреса № 4 подвал:",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'adress4'
    )
);
    
 

// Секция подвала сайта
$wp_customize->add_section(
    // ID
    'data_footer_section',
    // Arguments array
    array(
        'title' => 'Данные соц сетей',
        'capability' => 'edit_theme_options',
        'description' => "Тут можно указать данные сайта"
    )
);



$wp_customize->add_setting(
    // ID
    'theme_ftext',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'theme_ftext_control',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Ссылка на Facebook",
        'section' => 'data_footer_section',
        // This last one must match setting ID from above
        'settings' => 'theme_ftext'
    )
);

$wp_customize->add_setting(
    // ID
    'theme_vtext',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'theme_vtext_control',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Ссылка на Vk",
        'section' => 'data_footer_section',
        // This last one must match setting ID from above
        'settings' => 'theme_vtext'
    )
);

$wp_customize->add_setting(
    // ID
    'theme_gtext',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'theme_gtext_control',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Ссылка на G+",
        'section' => 'data_footer_section',
        // This last one must match setting ID from above
        'settings' => 'theme_gtext'
    )
);
  
$wp_customize->add_setting(
    // ID
    'theme_tvtext',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'theme_tvtext_control',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Ссылка на Твитер",
        'section' => 'data_footer_section',
        // This last one must match setting ID from above
        'settings' => 'theme_tvtext'
    )
);    
    
}
add_action( 'customize_register', 'mytheme_customize_register' );
add_theme_support( 'custom-logo' );
function add_classes_on_li($classes, $item, $args) {
  $classes[] = 'list__item';
  return $classes;
}
add_filter('nav_menu_css_class','add_classes_on_li',1,3);

function grab_ids_from_gallery() {

global $post;
$attachment_ids = array();
$pattern = get_shortcode_regex();
$ids = array();

if (preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches ) ) {   //finds the     "gallery" shortcode and puts the image ids in an associative array at $matches[3]
$count=count($matches[3]);      //in case there is more than one gallery in the post.
for ($i = 0; $i < $count; $i++){
    $atts = shortcode_parse_atts( $matches[3][$i] );
    if ( isset( $atts['ids'] ) ){
    $attachment_ids = explode( ',', $atts['ids'] );
    $ids = array_merge($ids, $attachment_ids);
    }
}
}
  return $ids;

 }
add_action( 'wp', 'grab_ids_from_gallery' );

// numbered pagination
function pagination($pages = '', $range = 4)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         
        
			
         
         
         echo "<div class=\"pagination\"><ul class=\"pagination__list\">";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class=\"active\"><a href=\"#\">".$i."</a></li>":"<li> <a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>";
             }
         }
  echo "</ul>";
         if ($paged >1){
         echo "<a href=\"".get_pagenum_link($paged - 1)."\" class=\"link__page page__prev\">&lt;&nbsp;Назад</a>";
             }
         if ($paged < $i-1){
		 echo "<a href=\"".get_pagenum_link($paged + 1)."\" class=\"link__page page__next\">Вперед&nbsp;&gt;</a>";}
         /*if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";*/
         echo "</div>\n";
     }
}

