<?php
/*
Template Name: Контакты
*/

get_header(); ?>
    <div class="header__main">
			<div class="container">
				<div class="main__title main__title-inner main__title-contact">
					<h2>Свяжитесь с нами</h2>
				</div>

				<div class="contact__item">
					<a href="#" class="link__contact"><img src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/contact-1.png" alt=""></a>
					<span>найти</span>
					<p><?php echo get_option('adress'); ?></p>
				</div>

				<div class="contact__item">
					<a href="mailto:<?php echo get_option('email'); ?>" class="link__contact"><img src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/contact-2.png" alt=""></a>
					<span>написать</span>
					<p><?php echo get_option('email'); ?></p>
				</div>

				<div class="contact__item">
					<a href="tel:<?php echo get_option('site_telephone1'); ?>" class="link__contact"><img src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/img/contact-3.png" alt=""></a>
					<span>позвонить</span>
					<p><?php echo get_option('site_telephone1'); ?></p>
				</div>
			</div>
		</div>
</header>

	<div class="contact__map">
		<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=ayuAVYmsZMGvFsz5Uz1SJTM05mp_7R7E&amp;width=100%25&amp;height=580&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=false"></script>
	</div>

	<section class="order">
		<div class="container">
			<div class="order__title">Хотите заказать себе праздник?</div>
			<a href="#callback-form" class="order__link popup">Заказать</a>
		</div>
	</section>
<?php get_footer(); ?>



