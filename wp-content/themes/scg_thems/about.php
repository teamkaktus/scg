<?php
/*
Template Name: О нас
*/

get_header(); ?>

			
	
	

		<div class="header__main">
			<div class="container">
				<div class="main__title main__title-inner main__title-about">
				<?php the_title( '<h2>', '</h2>' ); ?>
				</div>
			</div>
		</div>
	</header>

	<section class="about clearfix">
		<div class="container">
			<div class="about__content">
				<?php the_post(); the_content(); ?>
			</div>

			<div class="about__sidebar">
				<div class="about__team">
					<span class="team__title">наша команда</span>
       <?php 
            $wp_query = new WP_Query(
                                array( 'cat'  => '6' ));
    
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                      <div class="team__item">
						<img src="<?php echo the_post_thumbnail_url(full); ?>" alt="">
						<span class="team__name"><?php the_title(); ?></span>
					</div>
                               
                         
        <?php  endwhile; ?>
        <?php wp_reset_postdata(); ?>
       
				</div>
				<div class="about__team" style="    text-align: center;">
				 <a href="<?php echo esc_url( get_site_url() ); ?>/novosti/" class="newsbutton">НОВОСТИ</a>
				 </div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>



