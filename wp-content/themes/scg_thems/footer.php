	<footer>
		<div class="container">
			<ul class="footer__menu">
			<?php

                        $args = array(
	'theme_location'  => '', // область темы
	'menu'            => 'меню низ', // какое меню нужно вставить (по порядку: id, ярлык, имя)
	'container'       => false,
	'menu_class'      => '',
	'echo'            => true, // вывести или записать в переменную
	'items_wrap'      => '<ul class=\"%2$s\">%3$s</ul>', // HTML-шаблон
	'depth'           => 0 // количество уровней вложенности
);
                        wp_nav_menu($args);
                        ?>
			</ul>

			<div class="social__block">
				<a href="<?php echo get_option('theme_ftext'); ?>"  class="social__link social__link-1">facebook</a>
				<a href="<?php echo get_option('theme_vtext'); ?>"  class="social__link social__link-2">vkontakte</a>
				<a href="<?php echo get_option('theme_gtext'); ?>"  class="social__link social__link-3">google+</a>
				<a href="<?php echo get_option('theme_tvtext'); ?>" class="social__link social__link-4">twitter</a>
			</div>

			<div class="address__item">
				<span>1</span>
				<p><?php echo get_option('city1'); ?><br><?php echo get_option('adress1'); ?>	</p>
			</div>

			<div class="address__item">
				<span>2</span>
				<p><?php echo get_option('city2'); ?><br><?php echo get_option('adress2'); ?>	</p>
			</div>

			<div class="address__item">
				<span>3</span>
				<p><?php echo get_option('city3'); ?><br><?php echo get_option('adress3'); ?>	</p>
			</div>

			<div class="address__item">
				<span>4</span>
				<p><?php echo get_option('city4'); ?><br><?php echo get_option('adress4'); ?>	</p>
			</div>

			<span class="map__marker">1</span>
			<span class="map__marker2">2</span>
			<span class="map__marker3">3</span>
			<span class="map__marker4">4</span>
		</div>
	</footer>


	<div class="hidden">
		<div id="callback-form" class="callback__form mfp-with-anim">
			<h3 class="form__title">Заказать обратный звонок</h3>
			<form id="form">
                <span style="color:red" id="firstnamef"></span><span style="color:#0062ff" id="firstname"></span>
				<input name="name" type="text" placeholder="Ваше имя">
				<input name="phone" type="text" placeholder="Ваш e-mail">
				<input name="email" type="text" placeholder="Контактный телефон">
				<input name="emailtosend" type="text" value="<?php echo get_option('email'); ?>" style=" display: none;
">
				<button type="button" onclick="sendmail()" class="form__btn">Заказать</button>
			</form>
		</div>


<?php //if (is_page_template('meropriat.php')) { ?>
 <?php
            $wp_query = new WP_Query(
                                array( 'cat'  => '4',
                                       'posts_per_page' => 10
                                   ));


        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                   <?php
                $prev_post = get_adjacent_post( true, '', true );
                $next_post = get_adjacent_post( true, '', false );
                $prev_post_id = $prev_post->ID;
                $next_post_id = $next_post->ID;
?>
                     <div id="ev-modal<?php echo get_the_ID(); ?>" class="event__modal mfp-with-anim">
			<div class="nav__modal">
			<?php if($next_post_id){?>
				<button  href="#ev-modal<?php echo $next_post_id; ?>" class="nav__btn prev__mod popup"><i></i>Предыдущее мероприятие</button>
				<?php } ?>
				<?php if($prev_post_id){?>
				<button href="#ev-modal<?php echo $prev_post_id; ?>" class="nav__btn next__mod popup">Следующее мероприятие<i></i></button>
				<?php } ?>
			</div>
			
			<h2 class="modal__title"><?php the_title(); ?></h2>
			<div class="modal__descr">
				<?php
                            the_content(); 
                    ?>
			</div>

                    
                    
                    <ul class="slides">
          <?php $image = get_field('_слайд_шоу_изображения1'); if( !empty($image) ): ?>           
    <input type="radio" name="radio-btn" id="img-1" checked />
    <li class="slide-container">
		<div class="slide" style="    transform: scale(1); opacity: 1;">
			<img src="<?php echo get_field( "_слайд_шоу_изображения1" );?>" />
        </div>
    </li>
     <?php endif; ?>
 <?php $image = get_field('_слайд_шоу_изображения2'); if( !empty($image) ): ?>
    <input type="radio" name="radio-btn" id="img-2" />
    <li class="slide-container">
        <div class="slide">
          <img src="<?php echo get_field( "_слайд_шоу_изображения2" );?>" />
        </div>
    </li>
     <?php endif; ?>
 <?php $image = get_field('_слайд_шоу_изображения3'); if( !empty($image) ): ?>
    <input type="radio" name="radio-btn" id="img-3" />
    <li class="slide-container">
        <div class="slide">
          <img src="<?php echo get_field( "_слайд_шоу_изображения3" );?>" />
        </div>
		<div class="nav">
			<label for="img-2" class="prev">&#x2039;</label>
			<label for="img-4" class="next">&#x203a;</label>
		</div>
    </li>
     <?php endif; ?>
 <?php $image = get_field('_слайд_шоу_изображения4'); if( !empty($image) ): ?>
    <input type="radio" name="radio-btn" id="img-4" />
    <li class="slide-container">
        <div class="slide">
          <img src="<?php echo get_field( "_слайд_шоу_изображения4" );?>" />
        </div>
    </li>
     <?php endif; ?>
 <?php $image = get_field('_слайд_шоу_изображения5'); if( !empty($image) ): ?>
    <input type="radio" name="radio-btn" id="img-5" />
    <li class="slide-container">
        <div class="slide">
          <img src="<?php echo get_field( "_слайд_шоу_изображения5" );?>" />
        </div>
    </li>
 <?php endif; ?>

 <?php $image = get_field('_слайд_шоу_изображения1'); if( !empty($image) ): ?>    
    <li class="nav-dots">
      <label for="img-1" class="nav-dot" id="img-dot-1"></label>
      <?php $image = get_field('_слайд_шоу_изображения2'); if( !empty($image) ): ?>    
      <label for="img-2" class="nav-dot" id="img-dot-2"></label>
       <?php endif; ?>       
      <?php $image = get_field('_слайд_шоу_изображения3'); if( !empty($image) ): ?>    
      <label for="img-3" class="nav-dot" id="img-dot-3"></label>
       <?php endif; ?>       
      <?php $image = get_field('_слайд_шоу_изображения4'); if( !empty($image) ): ?>    
      <label for="img-4" class="nav-dot" id="img-dot-4"></label>
       <?php endif; ?>       
      <?php $image = get_field('_слайд_шоу_изображения5'); if( !empty($image) ): ?>    
      <label for="img-5" class="nav-dot" id="img-dot-5"></label>
       <?php endif; ?>       
    </li>
  <?php endif; ?>          
            </ul>
		</div>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); ?>







<?php //} ?>

	</div>

	<div class="loader">
		<div class="loader_inner"></div>
	</div>

	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/html5shiv/es5-shim.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/html5shiv/html5shiv.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/respond/respond.min.js"></script>
	<![endif]-->

	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/jquery/jquery-1.12.4.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/magnific/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/masonry/masonry.min.js"></script>
	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/libs/owlcarousel/owl.carousel.min.js"></script>

	<script src="<?php echo esc_url( get_site_url() ); ?>/wp-content/themes/scg_thems/js/common.js"></script>

</body>
</html>
