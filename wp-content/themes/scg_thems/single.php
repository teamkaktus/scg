<?php
/**
 * The template for displaying all single posts.
 *
 * @package Magnus
 */

get_header(); ?>
</header>

	<section class="service clearfix">
		<div class="container">

			<div class="service__sidebar">
		<div class="menu__block menu__block-service menu__block-1">
					<span class="menu__title menu__title-service">мы делаем</span>
					<ul class="menu__list menu__list-service menu__list-1">
						<li class="list__item list__item-1"><a href="<?php echo esc_url( get_site_url() ); ?>/promo/">Промо</a></li>
						<li class="list__item list__item-2"><a href="<?php echo esc_url( get_site_url() ); ?>/koncerty/">Концерты</a></li>
						<li class="list__item list__item-3"><a href="<?php echo esc_url( get_site_url() ); ?>/korporativy/">Корпоративы</a></li>
						<li class="list__item list__item-4"><a href="<?php echo esc_url( get_site_url() ); ?>/prazdniki/">Праздники</a></li>
						<li class="list__item list__item-5"><a href="<?php echo esc_url( get_site_url() ); ?>/kreativ/">Креатив</a></li>
						<li class="list__item list__item-6"><a href="<?php echo esc_url( get_site_url() ); ?>/vizualizaciya/">Визуализация</a></li>
						
						
					</ul>
				</div>

				<div class="menu__block menu__block-service menu__block-2">
					<span class="menu__title menu__title-service">мы предоставляем</span>
					<ul class="menu__list menu__list-service menu__list-2">
				        <li class="list__item list__item-7"><a href="<?php echo esc_url( get_site_url() ); ?>/zvuk/">Звук</a></li>
						<li class="list__item list__item-8"><a href="<?php echo esc_url( get_site_url() ); ?>/svet/">Свет</a></li>
						<li class="list__item list__item-9"><a href="<?php echo esc_url( get_site_url() ); ?>/sceny/">Сцены</a></li>
						<li class="list__item list__item-10"><a href="<?php echo esc_url( get_site_url() ); ?>/proektory/">Проекторы</a></li>
						<li class="list__item list__item-11"><a href="<?php echo esc_url( get_site_url() ); ?>/speceffekty/">Спецэффекты</a></li>
						<li class="list__item list__item-12"><a href="<?php echo esc_url( get_site_url() ); ?>/energosnabzhenie/">Энергоснабжение</a></li>
					</ul>
				</div>
			</div>

			<div class="service__content" >
			<?php the_title( '<h1 class="page__title page__title-service" style="width: 560px;">', '</h1>' ); ?>
				<div class="service__descr" style="    padding-top: 120px;">
					<?php
                            the_post();
                            the_content(); 
                    ?>
				</div>
			</div>
		</div>
	</section>

	
<?php get_footer(); ?>
