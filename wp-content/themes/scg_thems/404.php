<?php get_header(); ?>
                      
                    
                      <div class="header__main">
			<div class="container">
				

				   <div class="dtcell" style="color: #fff;
    text-align: center;
    height: 100%;
    vertical-align: top;
    padding: 100px 0;">
                        <h1 class="page-404-title" style="margin: auto;
    font-size: 225px;
    line-height: 1;">404</h1>
                        <h4 class="page-404-sub-title" style="margin: -12px auto 47px;
    font-size: 36px;
    line-height: 1.111111111111111;
    max-width: 530px;">PAGE NOT FOUND</h4>
                        <p class="page-404-text">
                            The link you clicked may be broken or the page may
                            have been removed.
                        </p>
                    </div>
			</div>
		</div>
</header>
<?php get_footer(); ?>