<?php get_header(); ?>
		<div class="header__main">
			<div class="container">
				<div class="main__title">
					<h1 style="width: 640px;"><?php  echo get_bloginfo('description'); ?></h1>
				</div>
			</div>
		</div>
	</header>
	<section class="main clearfix">
		<div class="container">
			<div class="main__content">

				<div class="menu__block menu__block-1">
					<span class="menu__title">мы<br>делаем</span>
					<ul class="menu__list menu__list-1">
						<li class="list__item list__item-1"><a href="<?php echo esc_url( get_site_url() ); ?>/promo/">Промо</a></li>
						<li class="list__item list__item-2"><a href="<?php echo esc_url( get_site_url() ); ?>/koncerty/">Концерты</a></li>
						<li class="list__item list__item-3"><a href="<?php echo esc_url( get_site_url() ); ?>/korporativy/">Корпоративы</a></li>
						<li class="list__item list__item-4"><a href="<?php echo esc_url( get_site_url() ); ?>/prazdniki/">Праздники</a></li>
						<li class="list__item list__item-5"><a href="<?php echo esc_url( get_site_url() ); ?>/kreativ/">Креатив</a></li>
						<li class="list__item list__item-6"><a href="<?php echo esc_url( get_site_url() ); ?>/vizualizaciya/">Визуализация</a></li>
					</ul>
				</div>

				<div class="menu__block menu__block-2">
					<span class="menu__title">мы<br>предоставляем</span>
					<ul class="menu__list menu__list-2">
					 <li class="list__item list__item-7"><a href="<?php echo esc_url( get_site_url() ); ?>/zvuk/">Звук</a></li>
						<li class="list__item list__item-8"><a href="<?php echo esc_url( get_site_url() ); ?>/svet/">Свет</a></li>
						<li class="list__item list__item-9"><a href="<?php echo esc_url( get_site_url() ); ?>/sceny/">Сцены</a></li>
						<li class="list__item list__item-10"><a href="<?php echo esc_url( get_site_url() ); ?>/proektory/">Проекторы</a></li>
						<li class="list__item list__item-11"><a href="<?php echo esc_url( get_site_url() ); ?>/speceffekty/">Спецэффекты</a></li>
						<li class="list__item list__item-12"><a href="<?php echo esc_url( get_site_url() ); ?>/energosnabzhenie/">Энергоснабжение</a></li>
					</ul>
				</div>


			</div>
<?php $videoimg = get_the_post_thumbnail( 27, 'full' );  
      $videourl =  get_post(27); 
      $videourl =  $videourl->post_excerpt;
         
           ?>
			<div class="main__sidebar">
				<div class="sidebar__video">
					
					<div class="video__preview">
						<a href="<?php echo $videourl; ?>" class="popup__video" data-effect="mfp-zoom-in" class="popup__video"><?php echo $videoimg; ?></a>
					</div>
				</div>
						
					
				
				
				<div class="sidebar__banner">
					<div class="banner__slider">
					<?php $slidimage = get_attached_media('image', 22 ); ?>
					<?php foreach ($slidimage as $medias){ ?>
						<div class="slider__item">
							<img src="<?php echo $medias->guid; ?>" alt="">
						</div>

				<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="clients">
		<div class="container">
			<h2 class="section__title section__title-clients">наши клиенты</h2>
<?php $slidimagek = get_the_post_thumbnail( 30, 'full' ); ?>
			<div class="clients__list">
				<?php echo $slidimagek; ?>
			</div>
		</div>
	</section>

	<section class="events clearfix">
		<div class="container">
			<h2 class="section__title section__title-events">Наши мероприятия</h2>
        <?php 
            $class=array("event__item-bg1", "event__item-bg2", "event__item-bg3", "event__item-bg3", "event__item-bg1", "event__item-bg2");
            $wp_query = new WP_Query(
                                array( 'cat'  => '4',
                                       'posts_per_page' => 6
                                   ));
           $i=0;
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                       <div class="event__item <?php echo $class[$i]; ?>">
				<img src="<?php echo the_post_thumbnail_url(full); ?>" alt="" style="min-width: 380px;">
				<div class="event__caption" style="    width: 300px;"><?php the_title(); ?></div>
				<a href="<? echo get_permalink(); ?>" class="event__link">link</a>
			</div>             
                         
        <?php $i++; endwhile; ?>
        <?php wp_reset_postdata(); ?>
			
		</div>
	</section>
		<section class="order">
		<div class="container">
			<div class="order__title">Хотите заказать себе праздник?</div>
			<a href="#callback-form" class="order__link popup">Заказать</a>
		</div>
	</section>
<?php get_footer(); ?>
