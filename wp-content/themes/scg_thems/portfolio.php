<?php
/*
Template Name: Портфолио
*/

get_header(); ?>

	</header>

	<section class="blog clearfix">
		<div class="container">
			<h1 class="page__title">Новости</h1>
		<?php 
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $wp_query = new WP_Query(
                                array( 'cat'  => '7',
                                       'posts_per_page' => 3,
                                      'paged'          => $paged
                                   ));
     
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    

			
			
			

			<div class="blog__item">
				<div class="item__pict">
					<img src="<?php echo the_post_thumbnail_url(full); ?>" alt="">
				</div>
				<div class="item__descr">
					<a href="<? echo get_permalink(); ?>"><h2 class="descr__title"><?php the_title(); ?></h2></a>
					<span class="descr__date"><?php echo get_the_date(); ?></span>
					<p><?php echo the_excerpt(); ?></p>
					<a href="<? echo get_permalink(); ?>" class="link__more">Подробнее</a>
				</div>
			</div>

		        
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

<?php if (function_exists("pagination")) {
    pagination($custom_query->max_num_pages);
} ?>


		</div>
	</section>
	
			
<?php get_footer(); ?>



